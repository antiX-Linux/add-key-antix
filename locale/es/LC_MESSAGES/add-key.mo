��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �     �          	          7  -   S  b   �     �     �          !                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Amigo, 2022
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 agregar Agregar elementos Cambiando las teclas por: Línea a remover Ninguna línea seleccionada: Remover Remover elementos Seleccione la primera tecla Seleccione la segunda tecla Esa combinación de teclas está siendo usada No hay ningún archivo ~/.%s/keys 
 En la sesión, DESKTOP_CODE='%s' 
 no coincide con su sistema. add-key La orden ha sido agregada la línea ha sido removida tercera tecla (alfanumérica) 