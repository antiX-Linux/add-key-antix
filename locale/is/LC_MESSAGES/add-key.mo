��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �  
   �     �     �     �     �  
             -     D  *   W  q   �     �     �       &   9                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>, 2018
Language-Team: Icelandic (http://www.transifex.com/anticapitalista/antix-development/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Bæta við Bæta við atriðum Breyti lyklum fyrir:  Lína sem á að fjarlægja Engin lína valin Fjarlægja Fjarlægja atriði Veldu fyrsta lykilinn: Veldu annan lykil: Þessi lyklasamsetning er þegar í notkun Það er engin skrá ~/.%s/keys 
 Skilgreining DESKTOP_CODE='%s' í setunni 
 samsvarar ekki rétt kerfinu þínu add-key skipun hefur verið bætt við lína hefur verið fjarlægð þriðji lykill (bókstafur eða tala) 