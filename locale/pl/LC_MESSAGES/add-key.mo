��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  ^  �     )     /  "   >     a     u     �     �     �     �  &   �  M   �     C     K     e  !   ~                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: MX Linux Polska <mxlinuxpl@gmail.com>, 2019
Language-Team: Polish (http://www.transifex.com/anticapitalista/antix-development/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Dodaj Dodaj elementy Zmiana skrótów klawiszowych dla: Linia do usunięcia Nie wybrano linii: Usuń Usuń elementy Wybierz pierwszy klawisz: Wybierz drugi klawisz: Ta kombinacja klawiszy została użyta Brak pliku ~/.%s/keys 
DESKTOP_CODE='%s' sesji
nie pasuje do twojego systemu. add-key polecenie zostało dodane linia została usunięta trzeci klawisz (litera lub cyfra) 