��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  	  �     �     �     �     
          9     B     U     r  (   �  a   �          (     B     ^                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Roberto Saravia <saravia.jroberto@gmail.com>, 2014
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Añadir Añadir Elementos Cambiando las teclas para:  Línea a Eliminar Ninguna línea seleccionada: Eliminar Eliminar elementos Seleccione la primera tecla: Seleccione la segunda tecla: Esa combinación de teclas ha sido usada No hay ningún archivo ~/.%s/keys 
 En la sesión, DESKTOP_CODE='%s' 
 no coincide con su sistema añadir tecla La orden ha sido añadida la línea ha sido eliminada tercera tecla (letra o numero) 