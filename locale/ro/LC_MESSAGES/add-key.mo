��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �          $     4     O     h  3   �  z   �     1     ?     Y  "   u                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: mariusbaesu <marius.baiesu@gmail.com>, 2018
Language-Team: Romanian (http://www.transifex.com/anticapitalista/antix-development/language/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Adaugă Adaugă elemente Se schimbă cheile pentru: Linia de îndepărtat Nicio linie selectată: Îndepărtează Îndepărtează elementele Selectează prima cheie: Selectează a doua cheie: Această combinație de taste a mai fost utilizată Nu există niciun fișier ~/.%s/chei 
 Pentru această sesiune DESKTOP_CODE='%s' 
nu se potrivește corect cu sistemul dvs adaugă-cheie comanda a fost adăugată linia a fost îndepărtată a treia cheie (literă sau cifră) 