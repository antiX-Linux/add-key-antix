��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �  C   �  0   �  :   (     c     s  %   �  .   �  ?   �  �        �  6   �  *   �  X                     	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Pongpeera Wongprasitthiporn, 2020
Language-Team: Thai (http://www.transifex.com/anticapitalista/antix-development/language/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 เพิ่ม เพิ่มไอเทม กำลังเปลี่ยนคีย์สำหรับ: บรรทัดที่จะลบออก ไม่มีบรรทัดที่เลือก: ลบออก ลบไอเทม เลือกคีย์แรก: เลือกคีย์ที่สอง: คีย์ชุดนั้นถูกใช้แล้ว ไม่พบไฟล์ ~/.%s/keys 
 DESKTOP_CODE='%s' ของเซสชัน
ไม่ตรงกับระบบของคุณ add-key คำสั่งถูกเพิ่มแล้ว บรรทัดถูกลบออก คีย์ที่สาม (ตัวอักษร หรือตัวเลข) 