��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �  	   �     �     �     �     �     �               /      K  g   l     �     �     �  "                     	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Vanhoorne Michael, 2022
Language-Team: Dutch (Belgium) (http://www.transifex.com/anticapitalista/antix-development/language/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Toevoegen Items toevoegen wijzigen van sleutels voor: Te verwijderen regel Geen regel geselecteerd: Verwijderen Verwijderd items Selecteer de eerste sleutel Selecteer de tweede sleutel Deze combinatie is niet mogelijk Er is geen bestand ~/.%s/keys
De sessie variabele DESKTOP_CODE='%s' 
correspondeert niet met je systeem add-key commando toegevoegd regel is verwijderd derde sleutel (letters of nummers) 