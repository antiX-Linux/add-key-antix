��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �               '     8     R  0   n  r   �          !     >     W                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2019
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Aggiungi Aggiungi elementi Cambiamento tasti per: Riga da rimuovere Nessuna riga selezionata: Rimuovi Rimuovi elementi Seleziona il primo tasto: Seleziona il secondo tasto: Quella combinazione di tasti è già stata usata Non c'è nessun file ~/.%s/tasti 
Il DESKTOP_CODE='%s' della sessione
non corrisponde correttamente al tuo sistema aggiungi-tasto il comando è stato aggiunto la riga è stata rimossa terzo tasto (lettera o numero) 