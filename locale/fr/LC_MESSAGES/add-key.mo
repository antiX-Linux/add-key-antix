��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �      �  -   �     	       	   8  %   B      h      �  &   �  t   �     F     T     q  &   �                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (http://app.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Ajouter Associez des touches à combiner Changement de combinaisons de touches pour :  Ligne à supprimer Aucune ligne sélectionnée Supprimer Supprimez des combinaisons de touches Choisissez la première touche : Choisissez la deuxième touche : Cette combinaison est déjà utilisée Il n’existe pas de fichier ~/.%s/keys
Le DESKTOP_CODE de la session = « %s »
ne correspond pas à votre système Ajout-clavier La commande a été ajoutée La ligne a été supprimée Troisième touche (lettres ou nombres) 