��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �    �  	   �     �               -     L     T     b       *   �  �   �     F     W     q  !   �                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020-2021,2023
Language-Team: Portuguese (Brazil) (http://app.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Adicionar Adicionar Itens Alterar as teclas para: Linha para remover Nenhuma linha foi selecionada: Remover Remover Itens Selecionar a primeira tecla: Selecionar a segunda tecla Esta combinação de teclas já foi usada  O arquivo ~/.%s/keys não existe
O DESKTOP_CODE=‘%s’ da sessão
não corresponde corretamente com o 
seu sistema operacional Adicionar Atalho o comando foi adicionado  A linha foi removida terceira tecla (letra ou número) 