��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �     �               #     A  0   ^  d   �     �            !   -                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Engadir Engadir items Cambiando o atallo por: Liña a eliminar Ningunha liña seleccionada: Eliminar Eliminar items Seleccionar a primeira tecla: Seleccionar a segunda tecla: Esta combinación de teclas xa está para usarse O ficheiro ~/.%s/keys non existe
O DESKTOP_CODE='%s' da sesión
non combina correctamente co sistema engadir atallo Engadiuse este comando Eliminouse a liña terceira tecla (letra ou número) 