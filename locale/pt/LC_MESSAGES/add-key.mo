��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �    �  	   �     �     �     �          )     1     ?     ]  -   z  i   �          $     =  !   R                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2016-2019
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Adicionar Adicionar itens Alterar atalhos do:  Linha a remover Nenhuma linha seleccionada: Remover Remover itens Seleccionar a primeira tecla: Seleccionar a segunda tecla: Esta combinação de teclas já está em uso  O ficheiro ~/.%s/keys não existe
O DESKTOP_CODE='%s' da sessão
não combina correctamente com o sistema Adicionar Atalhos O comando foi adicionado A linha foi removida terceira tecla (letra ou número) 