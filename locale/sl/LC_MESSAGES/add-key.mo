��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �    �     �     �     �     �          -     6     H     [  (   o  W   �     �     �     
  "   $                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019,2021
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Dodaj Dodaj elemente Spreminjanje tipk za: Vrstica za brisanje Nobena vrstica ni izbrana: Izbriši Izbriši predmete Izberi prvo tipko: Izberi drugo tipko: Uporabljena je bila ta kombinacija tipk  Ni datoteke ~/.%s/keys 
 DESKTOP_CODE='%s' trenutne seje
 ni skladna z vašim sistemom. add-key ukaz je bil dodan vrstica je bila izbrisana tretja tipka (črka ali številka) 