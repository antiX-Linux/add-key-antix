��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �     �  	   �               ,  -   E  m   s     �     �       "                     	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Martin O'Connor <mar.oco@arcor.de>, 2017
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Hinzufügen Punkte hinzufügen Ändern von Schlüsseln für: Zeile entfernen Keine Zeile ausgewählt Entfernen Punkte entfernen Wähle die erste Taste: Wähle die zweite Taste: Diese Tastenkombination wurde schon verwendet Es existiert keine Datei ~/.%s/keys
Die Sitzungs-Variable DESKTOP_CODE='%s'
ist nicht korrekt für Ihr System hinzufügen des Schlüssel Befehl hinzugefügt Zeile entfernt dritte Taste (Buchstabe oder Zahl) 