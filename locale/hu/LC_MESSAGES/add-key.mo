��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     y     �  ,   �     �     �     �        "     $   :  3   _  V   �     �     �       &                     	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-15 19:09+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (http://www.transifex.com/anticapitalista/antix-development/language/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
 Hozzáadás Elemek hozzáadása Billentyűkombinációk beállítása ehhez: Eltávolítandó sor Nincs sor kijelölve: Eltávolítás Elemek eltávolítása Válassza ki az első billentyűt: Válassza ki a második billentyűt: Ez a billentyűkombináció már használatban van. Nem létezik fájl ~/.%s/keys
A folyamat DESKTOP_CODE='%s'
nem egyezik a rendszerével add-key Parancs hozzáadva sor eltávolítva harmadik billentyű (betű vagy szám) 